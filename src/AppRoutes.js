import React, { lazy } from 'react';
import { Routes, Route } from 'react-router-dom';

const FormList = lazy(()=>import('C:/tasks/Form with Validation/form/src/components/FormList/FormList.js'))

function AppRoutes(){
    return (
        <Routes>
            <Route path='/' exact element={<FormList />} />
            {/* <Route path='/StaffUI' exact element={<StaffUI />} /> */}
        </Routes>
    );
}

export default AppRoutes;
