import React, { Suspense, lazy, useEffect } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import './App.css';
import records from './Records';

const AppRoute = lazy(() => import('./components/AppRoutes'));

function App() {
  useEffect(() => {
    localStorage.setItem('LoginData', JSON.stringify(records));
  }, [])
  return (
    <BrowserRouter>
      <Suspense fallback={<h1>Loading...</h1>}>
        <Routes>
          <Route path='*' element={<AppRoute />} />
        </Routes>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
