import React from 'react'
import './StudentUI.css'
import { useHistory, useNavigate } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { authenticatedData } from '../../Shared/Utils';
//MUI Imports
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#FBB03B',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));


function StudentUI() {
  const handlelogout = () => {
    localStorage.removeItem('loggedIn')
    localStorage.removeItem('loggedUser')
    navigate('/');
  }

  const loggedUserData = authenticatedData();
  const navigate = useNavigate();
  // console.log("Data", loggedUserData)

  useEffect(() => {
    if (loggedUserData === undefined || loggedUserData === null) {
      navigate('/')
    }
  }, [loggedUserData])

  let Data = JSON.parse(localStorage.getItem('Quiz'));
  // console.log(Data,"data")
  const [quizData, setQuizData] = useState(Data);

  const handleStartQuiz = (quizIndex) => {
    navigate(`/StudentQuizPage/${quizIndex}`);
  }

  const handleCheckResult = (quizIndex) => {
    navigate(`/StudentQuizPage/${quizIndex}`);
  }

  // const currentUser = JSON.parse(localStorage.getItem('loggeduser'))
  const activestudentID = loggedUserData.id;

  // console.log(currentUser)
  return (
    <div>
      <div className='header'>
        <div className='studentDispay'>
          <p className='StudentName'>Welcome - {loggedUserData?.name}</p>
        </div>
        <div>
          <button className='logoutButton' onClick={handlelogout}>Logout</button>
        </div>
      </div>
      <div className='StudentQuiz'>
        {console.log(quizData)}
        {(quizData !== null && quizData !== undefined) ? 
        (quizData.map((quiz, quizIndex) => {
          // console.log(quiz)
          const quizName = quiz.quizName;
          // console.log(quizName);  
          return (<Box sx={{ width: '100%' }} key={quizIndex}>
            <Stack spacing={2} >
              <Item className='quiz-item'>{quizName}  
                {(quiz.students[activestudentID] && quiz.students[activestudentID].quizCompleted  === true) ? (
                  <Button variant="outlined" onClick={() => { handleCheckResult(quizIndex) }}>
                    Check Result
                  </Button>
                ) : (
                  <Button variant="outlined" onClick={() => { handleStartQuiz(quizIndex) }}>
                    Start Quiz
                  </Button>)}
              </Item>
            </Stack>
          </Box>)
        })) : <></> } 

      </div>
    </div >
  )
}

export default StudentUI