import React from 'react'
import Swal from 'sweetalert2'
import { useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import { authenticatedData } from '../../Shared/Utils';
import './StudentQuizPage.css'


const students = () => ({
    studentID: 1,
    studentName: "",
    quizCompleted: false,
    answers: []
})

const quizData =
{
    quizName: "",
    createdBy: "",
    questions: [{
        qNo: 1,
        questionText: "",
        options: [{
            id: "",
            name: "",
            correctAnswer: false
        }]
    }],
    students: []
}


function StudentQuizPage() {

    const { index } = useParams();
    const navigate = useNavigate();

    //Login verification
    const loggedUserData = authenticatedData();
    useEffect(() => {
        if (loggedUserData === undefined || loggedUserData === null) {
            navigate('/')
        }
    }, [loggedUserData])

    const activestudentID = loggedUserData.id;

    const existingData = JSON.parse(localStorage.getItem('Quiz'));
    const [data, setData] = useState(quizData);
    // console.log({ data })
    // console.log("Data", loggedUserData)
    const quizIndex = existingData[index];
    const quizCompletedCheck = existingData[index].students[activestudentID]
    // console.log(quizCompletedCheck)
    const questionList = quizIndex.questions
    const totalQuestion = questionList.length
    console.log({ totalQuestion })
    const [score, setScore] = useState(0);
    const [studentAns, setStudentAns] = useState(students());
    // console.log("StudentObj", studentAns)

    const copyData = { ...studentAns };
    const existingStudentAns = copyData;
    // console.log(existingStudentAns)

    //Student Quiz Score For header
    useEffect(() => {
        var myResult = 0;
        questionList.map((question, questionIndex) => {

            if (!(quizCompletedCheck === null || quizCompletedCheck === undefined)) {
                question.options.map((option, optionIndex) => {
                    ((existingData[index].students[activestudentID].answers[questionIndex].answer === optionIndex) && option.correctAnswer) && myResult++;
                    setScore(myResult);
                    console.log(score);
                })
            }
        })
        console.log(myResult, 'myResult')
    }, [score])

    useEffect(() => {
        const copyData = studentAns;
        const studentName = loggedUserData.name;
        copyData.studentName = studentName;
        copyData.studentID = loggedUserData.id;
        copyData.quizCompleted = true;
    }, [])

    const [answeredOption, setansweredOption] = useState({})
    // console.log(answeredOption, "answers State")

    const handleAnsweredOption = (event, questionIndex, optionIndex) => {
        console.log("QuestionIndex", questionIndex, "optionIndex", optionIndex, "event", event);
        const answerObj = {
            questionNo: questionIndex,
            answer: optionIndex
        }
        setansweredOption({ ...answeredOption, [questionIndex]: answerObj })
        // console.log(finalAnswers, "finalAnswers")
    }

    // console.log(data)
    const handleSubmit = () => {
        const existingData = JSON.parse(localStorage.getItem('Quiz'));
        // console.log(existingData)
        const quizIndex = parseInt(index)
        const currentQuiz = existingData.filter((item, itemIndex) => {
            return quizIndex === itemIndex
        })
        // console.log(currentQuiz)
        let copyData = currentQuiz[0];
        // console.log(copyData, 'currentQuiz')
        studentAns.answers = answeredOption
        // console.log(answeredOption, "answeredOption")
        const totalQuestion = copyData.questions.length
        const studentOption = Object.entries(answeredOption)
        // console.log(studentOption, "studentOption", quizCompletedCheck)

        if (quizCompletedCheck === undefined) {
            if (studentOption.length === 0) {
                // alert("Please answer all questions")
                Swal.fire("Warning!", "Please answer all questions", "error");
            } else if (studentOption.length !== totalQuestion) {
                // alert("Please enter all questions")
                Swal.fire("Warning!", "Please enter all questions", "warning");
            } else if (studentOption.length !== 0) {
                // studentAns.quizCompleted = true;
                const studentIndex = studentAns.studentID
                // console.log(studentIndex)
                // console.log("quizIndex", quizIndex)
                let studentList = copyData.students
                studentList = ({ ...studentList, [studentIndex]: studentAns })
                // console.log(studentList, "studentList")
                copyData.students = studentList
                // console.log(copyData, "copyData")
                existingData[quizIndex] = copyData
                localStorage.setItem('Quiz', JSON.stringify(existingData));
                navigate('/StudentUI')
                Swal.fire("Completed!", "Succefully completed Quiz", "success");
            }
        }
    }

    const handleAnswerCheck = () => {
        navigate('/StudentUI');
    }

    return (
        <div className='wrapper'>
            <div className='btn-header d-flex justify-content-around'>
                <div className="py-2 h3">Quiz Name: {existingData[index].quizName}</div>
                {((!(quizCompletedCheck === null || quizCompletedCheck === undefined))) ? (<div className="py-2 h3">Your Score: {score} out of {totalQuestion}</div>) : (<></>)}
                <div className="py-2 h3">Student Name: {loggedUserData?.name}</div>
            </div>
            {
                questionList.map((question, questionIndex) => {
                    console.log(question)
                    return (
                        <div className='quizCRUD' key={questionIndex}>
                            <div key={questionIndex}>
                                <div className="question ml-sm-5 pl-sm-5 pt-2">
                                    <div className="py-2 h3"><b>{question.questionText}</b></div>
                                    <div className="ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3" id="options">
                                        {question.options.map((option, optionIndex) => {

                                            return (
                                                (quizCompletedCheck === null || quizCompletedCheck === undefined) ? (
                                                    <div key={optionIndex}>
                                                        <input type="radio"
                                                            checked={answeredOption[questionIndex]?.answer === optionIndex}
                                                            onChange={event => handleAnsweredOption(event.target.checked, questionIndex, optionIndex)}
                                                        />
                                                        <span className="checkmark"></span>
                                                        <label className="options" key={optionIndex}>{option.name}
                                                        </label><br></br>
                                                    </div>
                                                ) : (
                                                    <div key={optionIndex}>
                                                        <input type="radio"
                                                            disabled
                                                            checked={answeredOption[questionIndex]?.answer === optionIndex}
                                                            onChange={event => handleAnsweredOption(event.target.checked, questionIndex, optionIndex)}
                                                        />
                                                        {/* <span className="checkmark"></span> */}
                                                        <label className="options" key={optionIndex}>{option.name}
                                                        </label>
                                                        {(option.correctAnswer) ? (<div className='correctAnswer'><p>This is correct answer</p></div>
                                                        ) :
                                                            // option.studentAnsweredThis 
                                                            existingData[index].students[activestudentID].answers[questionIndex].answer === optionIndex
                                                            &&
                                                            (<div className='wrongAnswer'><p>Wrong answer</p></div>)}
                                                    </div>

                                                )
                                            )
                                        })}
                                    </div>

                                </div>
                            </div>

                        </div>

                    )
                })
            }
            <div className='btn d-flex justify-content-end'>
                {(quizCompletedCheck === null || quizCompletedCheck === undefined) ? (
                    <button className="btn btn-danger" onClick={handleSubmit}>Submit</button>
                ) :
                    (quizCompletedCheck.quizCompleted === true) ?
                        (<button className="btn btn-danger" onClick={handleAnswerCheck}>Finish</button>
                        ) : (
                            <button className="btn btn-danger" onClick={handleSubmit}>Submit</button>)
                }
            </div>

        </div >
    )

}

export default StudentQuizPage