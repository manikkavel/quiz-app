import React, { lazy } from 'react';
import { Routes, Route } from 'react-router-dom';
// import { useNavigate } from 'react-router-dom';


const LoginForm = lazy(() => import('./LoginForm/LoginForm'));
const StaffUI = lazy(() => import('./Staff/StaffUI/StaffUI'));
const StaffAddQuiz = lazy(() => import('./Staff/StaffAddQuiz/StaffAddQuiz'));
const StudentUI = lazy(() => import('./Student/StudentPage/StudentUI'));
const StudentQuizPage = lazy(() => import('./Student/StudentQuizPage/StudentQuizPage'));


function AppRoutes() {

    // const navigate = useNavigate();
    // const authenticatedData = () => {
    //     const data = JSON.parse(localStorage.getItem('loggedin'))
    //     navigate('/');
    //     return data;
    // }

    // const loggedUserData = authenticatedData();
    // console.log("Data", loggedUserData)

    // const authenticatedData = () => {
    //   const data = JSON.parse(localStorage.getItem('loggedin'))
    //   return data;
    // }

    // const loggedUserData = authenticatedData();
    // console.log("Data", loggedUserData)

    return (
        <Routes>
            <Route path='/' exact element={<LoginForm />} />
            <Route path='/StaffUI' exact element={<StaffUI />} />
            <Route path='/StaffAddQuiz' exact element={<StaffAddQuiz type="add" />} />
            <Route path='/StaffEditQuiz/:index' exact element={<StaffAddQuiz type="edit" />} />
            <Route path='/StudentUI' exact element={<StudentUI />} />
            <Route path='/StudentQuizPage/:index' exact element={<StudentQuizPage />} />
        </Routes>
    );
}

export default AppRoutes;
