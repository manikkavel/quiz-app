import React, { useState, useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom';
import { authenticatedData } from '../../Shared/Utils';
import './StaffAddQuiz.css';
// import QuestionAdd from './Add Questions/QuestionAdd';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';

const option = () => ({
    id: "",
    name: "",
    optionNameValidation: "",
    correctAnswer: false,
})
const question = () => ({
    questionText: "",
    questionTextValidation: "",
    options: [option()]
})
const quizData =
{
    quizName: "",
    quizNameValidation: "",
    createdBy: "",
    questions: [question()],
    students: {}
}


const StaffAddQuiz = ({ type }) => {
    const [data, setData] = useState(quizData)
    console.log(data, 'data')
    const { index } = useParams();
    const navigate = useNavigate()

    //To not redirect when the user is active
    const loggedUserData = authenticatedData();
    useEffect(() => {
        if (loggedUserData === undefined || loggedUserData === null) {
            navigate('/')
        }
    }, [loggedUserData])

    //-------------------Validation-----------------

    const checkValidation = () => {
        const copyData = { ...data }

        //QuizName Check
        if (!copyData.quizName.trim()) {
            copyData.quizNameValidation = "Quiz Name is required";
        } else {
            copyData.quizNameValidation = "";
        }

        //QuestionText Validation
        let questionNo;
        let questionEvery = true
        copyData.questions.map((question, questionIndex) => {

            if (question.questionText === "") {
                questionNo = questionIndex;
                questionEvery = false;
                // console.log(questionNo, "questionNo", questionEvery, "QuestionEvery")
            }
            else {
                copyData.questions[questionIndex].questionTextValidation = "";
            }
            if (questionEvery === false) {
                console.log(questionNo, "this question Text Added")
                console.log(copyData.questions[questionNo])
                copyData.questions[questionNo].questionTextValidation = "QuestionText cannot be empty";
                setData(copyData)
            }
        })


        //Option Validation
        let optionNo;
        let optionQuestionIndex;
        let optionEvery = true;
        copyData.questions.map((question, questionIndex) => {
            question.options.map((validateOptions, optionIndex) => {
                if (validateOptions.name === "") {
                    optionQuestionIndex = questionIndex
                    optionNo = optionIndex;
                    optionEvery = false;
                    // console.log(questionNo, "questionNo", optionNo, "optionNO", optionEvery, "optionEvery", questionEvery, "QuestionEvery")
                }
                else {
                    copyData.questions[questionIndex].options[optionIndex].optionNameValidation = "";
                }
                if (optionEvery === false) {
                    // console.log(questionNo, 'questionNO', optionNo, "optionNO", "this question Text Added")
                    copyData.questions[optionQuestionIndex].options[optionNo].optionNameValidation = "Option text cannot be empty";
                    setData(copyData)
                }
            })
        })
        const returnObj = { questionEvery, optionEvery }
        return returnObj
    }


    useEffect(() => {
        // console.log('entered useeffect')
        const currentUser = JSON.parse(localStorage.getItem('loggedUser'));
        const createdBy = currentUser.name
        let copyData = { ...data };
        copyData.createdBy = createdBy;
        // console.log(copyData)
        setData(copyData)
        let quizList = JSON.parse(localStorage.getItem('Quiz'));

        //If user is editing display the existing data
        if (type === 'edit') {
            const filteredData = quizList.filter((item, i) => parseInt(index) === i)
            copyData.quizName = filteredData[0].quizName
            copyData.questions = filteredData[0].questions
            // console.log({ copyData, filteredData })
            setData(copyData)
        }
    }, [])

    // console.log("data", data, "quizData", quizData)

    const handleSave = () => {
        const existingData = JSON.parse(localStorage.getItem('Quiz'));
        if (existingData !== null) {
            if (type === 'add') {
                checkValidation()
                const validationReturn = checkValidation()
                if (validationReturn.questionEvery === true && validationReturn.optionEvery === true) {
                    existingData.push(data);
                    // console.log("existingarray", existingData, "quizData", quizData)
                    // console.log("before setting data", data)
                    // setData(quizData)
                    // console.log("quizData", quizData)
                    // console.log("after setting data", data)
                    localStorage.setItem('Quiz', JSON.stringify(existingData));
                    navigate('/StaffUI');
                }
            } else if (type === 'edit') {
                const existingEditObj = existingData[index];
                existingEditObj.quizName = data.quizName;
                existingEditObj.questions = data.questions
                const editValidation = checkValidation()
                if(editValidation.questionEvery === true && editValidation.optionEvery === true){
                    localStorage.setItem('Quiz', JSON.stringify(existingData));
                    navigate('/StaffUI');
                }
            }
        } else {
            const array = []
            array.push(data);
            localStorage.setItem('Quiz', JSON.stringify(array));
            navigate('/StaffUI');
        }
    }

    const addQuestions = () => {
        const newData = {
            ...data, questions: [...data.questions, question()]
        }
        setData(newData)
    }

    const removeQuestion = (questionIndex) => {
        const questions = data.questions;
        const newData = {
            ...data, questions: questions.filter((item, itemIndex) => {
                return itemIndex !== questionIndex
            })
        }
        setData(newData)
    }

    const addOptions = (questionIndex) => {
        const NewQuestion = [...{ ...data }.questions];
        let updateQuestionOption = NewQuestion[questionIndex];
        updateQuestionOption = { ...updateQuestionOption, options: [...updateQuestionOption['options'], option()] };
        NewQuestion[questionIndex] = updateQuestionOption;
        const newData = {
            ...data, questions: NewQuestion
        }
        setData(newData)
    }

    const removeOption = (questionIndex, optionIndex) => {

        const NewQuestion = [...{ ...data }.questions];
        let updateQuestionOption = NewQuestion[questionIndex];
        const options = updateQuestionOption.options
        updateQuestionOption = {
            ...updateQuestionOption, options: options.filter((item, itemIndex) => {
                return itemIndex !== optionIndex
            })
        };
        NewQuestion[questionIndex] = updateQuestionOption;
        const newData = {
            ...data, questions: NewQuestion
        }
        setData(newData)
    }

    const handleChange = (key, value) => {
        const updatedData = {
            ...data, [key]: value
        }
        setData(updatedData)
    }

    const HandleQuestion = (e, index) => {
        const value = e.target.value;
        let copyData = { ...data, questions: data.questions.map(item => ({ ...item })) };
        copyData.questions[index].questionText = value;
        // console.log("copyData", copyData, "quizData", quizData)
        setData(copyData)
        // console.log("quizData", quizData)
    };

    const handleOptionChange = (key, value, questionIndex, optionIndex) => {
        console.log('key', key, 'value', value, 'questionIndex', questionIndex, 'optionIndex', optionIndex)
        const copyData = {
            ...data, questions: data.questions.map(item => ({
                ...item, options: item.options.map(item => ({ ...item }))
            }))
        };
        console.log(copyData, 'copyData')
        let option = copyData.questions[questionIndex].options[optionIndex]
        const totalOption = copyData.questions[questionIndex].options
        option[key] = value
        // const updatedOptions = {
        //     option, [key]: value
        // }
        if (key === 'correctAnswer') {
            const updatedCheckBox = totalOption.map((mappedOption, Index) => {
                if (optionIndex === Index) {
                    mappedOption.correctAnswer = value;
                }
                else {
                    mappedOption.correctAnswer = false;
                }
                return mappedOption
            })
            option = updatedCheckBox
            // setData(updatedCheckBox)
        }
        // console.log("option", option)
        setData(copyData);
    }


    return <div>
        <div className='AddQuizHeader'>
            <div><p className='ADD-QUIZTITLE'>Add Quiz</p></div>
            <div><input type="submit" className='StaffAddSAVE' onClick={handleSave}></input></div>
        </div>
        <div className='flexwrapper'>
            <div className='AddQuizNameTextField'>
                <label>Quiz Name :</label>
                <input placeholder='Eg, GK, Sports etc.' value={data.quizName} onChange={(e) => handleChange('quizName', e.target.value)} type='text' required />
                <p className='errorMessage'>{data.quizNameValidation}</p>
            </div>

            <div className='AddQuizBtn'>
                <button className='AddQuizButton' onClick={addQuestions} >Add Questions</button>
            </div>
        </div>
        <div className='AddQuizQuestionField'>
            <div className='Qno-Qtxt'>
                <b>Question No</b>
                {/* {console.log(data.questions, "data.questions", "data", data)} */}
                {data.questions.map((item, questionIndex) => {
                    return (
                        <div key={questionIndex}>
                            <div>
                                {questionIndex + 1} <br />
                                <textarea placeholder='Enter your question..' onChange={(e) => HandleQuestion(e, questionIndex)} value={item.questionText} required></textarea>
                                <IconButton aria-label="delete" onClick={() => removeQuestion(questionIndex)}>
                                    <DeleteIcon />
                                </IconButton>
                                <p className='errorMessage'>{data.questions[questionIndex].questionTextValidation}</p>
                            </div>
                            <button onClick={() => addOptions(questionIndex)}>AddOptions</button>
                            <div className='opt'>
                                <div className='optionGrid'>
                                    {/* {console.log(item.options, "options")} */}
                                    {item.options.map((option, optionIndex) => {
                                        return (
                                            <div key={optionIndex}>
                                                <div className='option'>
                                                    <div className='radioBtn'>
                                                        <input type="radio" checked={option.correctAnswer}
                                                            onChange={e => handleOptionChange('correctAnswer', e.target.checked, questionIndex, optionIndex)} />
                                                    </div>
                                                    <div className='optionInput'>
                                                        <input onChange={(event) => handleOptionChange('name', event.target.value, questionIndex, optionIndex)} value={option.name} type='text' required />
                                                    </div>
                                                    {/* <button onClick={() => removeOption(questionIndex, optionIndex)}>Remove Option</button> */}
                                                    <IconButton aria-label="delete" onClick={() => removeOption(questionIndex, optionIndex)}>
                                                        <DeleteIcon />
                                                    </IconButton><br></br>
                                                </div>
                                                <p className='errorMessage'>{data.questions[questionIndex].options[optionIndex].optionNameValidation}</p>
                                            </div>
                                        )
                                    })}
                                </div>
                            </div >
                        </div>
                    )
                })}

                {/* //AddOptions
                    <AddOptions /> */}
            </div>
        </div>
    </div>
}

export default StaffAddQuiz