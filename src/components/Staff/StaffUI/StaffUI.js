import React, { useEffect,useState  } from 'react'

import { Link, useNavigate } from 'react-router-dom';
import { authenticatedData } from '../../Shared/Utils';

//MUI Imports
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { styled } from '@mui/material/styles';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';

import './StaffUI.css'

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#FBB03B',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

function StaffUI() {

  const handlelogout = () => {
    localStorage.removeItem('loggedIn')
    localStorage.removeItem('loggedUser')
    // console.log(localStorage.getItem('loggeduser'))
    navigate('/');
  }

  const loggedUserData = authenticatedData();
  const navigate = useNavigate()
  // console.log("Data", loggedUserData)

  useEffect(() => {
    if (loggedUserData === undefined || loggedUserData === null) {
      navigate('/')
    }
  }, [loggedUserData])


  let Data = JSON.parse(localStorage.getItem('Quiz'));
  const [quizData, setQuizData] = useState(Data);

  const handleDelete = (quizIndex) => {
    Data.map((quiz, Index) => {
      if (Index === quizIndex) {
        const quizList = [...Data];
        quizList.splice(quizIndex, 1);
        const newList = quizList;
        setQuizData(newList);
        console.log("Data Deleted",newList)
        localStorage.setItem('Quiz', JSON.stringify(newList))
        return newList
      }
    })
  }

  const handleEdit = (quizIndex) => {
    navigate(`/StaffEditQuiz/${quizIndex}`)
  }

  return (
    <div>
      <div className='header'>
        <div className='staffDispay'>
          <p className='StaffName'>Welcome - {loggedUserData.name}</p>
        </div>
        <div>
          <button className='logoutButton' onClick={handlelogout}>Logout</button>
        </div>
      </div>
      <Link to={'/StaffAddQuiz'}>
        <div className='questionadd'>
          <button className='Add-Questions'>Add Quiz</button>
        </div>
      </Link>
      <div className='quizCRUD'>
      {(quizData !== null && quizData !== undefined)? 
        (quizData.map((quiz, quizIndex) => {
          // console.log(quiz)
          const quizName = quiz.quizName;
          // console.log(quizName);  
          return (<Box sx={{ width: '100%' }} key={quizIndex}>
            <Stack spacing={2} >
              <Item className='quiz-item'>{quizName}
                <Button variant="outlined" startIcon={<DeleteIcon />} onClick={() => { handleDelete(quizIndex) }}>
                  Delete
                </Button>
                <Button variant="outlined" startIcon={<EditIcon />} onClick={() => { handleEdit(quizIndex) }}>
                  EDIT
                </Button>
              </Item>
            </Stack>
          </Box>)
        })) : <></> }
      </div>
    </div >
  )
}

export default StaffUI