import React, { useState, useEffect } from 'react';

// import { useHistory } from 'react-router';
import records from '../../Records';
// import quiz from '../../Quiz';
import {
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  // MDBInput,
  // MDBIcon
}
  from 'mdb-react-ui-kit';

import './LoginForm.css';
import { useNavigate } from 'react-router';
import { authenticatedData } from '../Shared/Utils';

function LoginForm() {

  const navigate = useNavigate();
  const loggedUserData = authenticatedData();
  const loginData = JSON.parse(localStorage.getItem('LoginData'));
  useEffect(() => {
    if (loginData === null && loginData === undefined) {
      localStorage.setItem('LoginData', JSON.stringify(records));
    }
    if (loggedUserData !== undefined && loggedUserData !== null) {
      if (loggedUserData.type === 'staff') {
        console.log(loggedUserData);
        navigate('/staffUI')
      } else if (loggedUserData.type === 'Student') {
        navigate('/StudentUI')
      }
      console.log(loggedUserData)
    }
  }, [loggedUserData])

  useEffect(() => {

    localStorage.setItem('LoginData', JSON.stringify(records));

  }, [])


  const records = JSON.parse(localStorage.getItem('LoginData'));
  const [Email, setEmail] = useState("");
  const [Pass, setPass] = useState("");

  const checkEmailPassword = (Email, Pass) => {
    const matchRecords = records.find((record) => `${record.emailid}` === Email && `${record.pass}`)
    return matchRecords
  }

  const handleEmail = (e) => {
    setEmail(e.target.value);
  }

  const handlePass = (e) => {
    setPass(e.target.value);
  }

  const handleSubmit = (e) => {
    const isUserMatchObj = checkEmailPassword(Email, Pass)
    // console.log({ isUserMatchObj })
    if (isUserMatchObj !== undefined) {
      if (isUserMatchObj.type === 'Staff') {
        localStorage.setItem("loggedIn", true)
        localStorage.setItem("loggedUser", JSON.stringify(isUserMatchObj))
        navigate('/staffUI')
      }
      else if (isUserMatchObj.type === 'Student') {
        localStorage.setItem("loggedIn", true)
        localStorage.setItem("loggedUser", JSON.stringify(isUserMatchObj))
        navigate('/StudentUI')
      }
    } else {
      alert("Wrong Password or Email!")
    }
  }

  return (
    <div className='body'>
      <MDBContainer fluid>

        <MDBRow className='d-flex justify-content-center align-items-center h-100'>
          <MDBCol col='12'>

            <MDBCard className='bg-white text-dark my-5 mx-auto' style={{ borderRadius: '1rem', maxWidth: '400px' }}>
              <MDBCardBody className='p-5 d-flex flex-column align-items-center mx-auto w-100'>

                <h3 className="fw-bold mb-2 text-uppercase wlcm">Welcome to Quiz!</h3>
                <h3 className="fw-bold mb-2 text-uppercase">Login</h3>
                <p className="text-dark-50 mb-4">Please enter your login and password!</p>

                {/* <MDBInput wrapperClass='mb-4 mx-5 w-100' labelClass='text-dark' label='Email address' id='formControlLg' type='email' size="lg" /> */}
                <div>
                  <div className='email'>
                  <label>Email :</label>
                  <input className='emailTextBox' type='text' placeholder='Enter your email' onChange={handleEmail} ></input>
                  </div>
                  <div className='password'>
                  <label>Password :</label>
                  <input className='passwordTextBox' type='text' placeholder='Enter your password' onChange={handlePass} ></input>
                  </div>
                </div>
                {/* <MDBInput wrapperClass='mb-4 mx-5 w-100' labelClass='text-dark' label='Password' id='formControlLg1' type='password' size="lg" /> */}


                <MDBBtn outline className='mx-2 px-5' color='white' size='lg' onClick={(e) => handleSubmit()}>
                  Login
                </MDBBtn>


              </MDBCardBody>
            </MDBCard>

          </MDBCol>
        </MDBRow>
      </MDBContainer>
    </div>
  );
}

export default LoginForm;