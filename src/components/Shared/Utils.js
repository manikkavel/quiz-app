export const authenticatedData = () => {
    const data = JSON.parse(localStorage.getItem('loggedUser'))
    return data;
}